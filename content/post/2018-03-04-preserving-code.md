---
title: Preserving the catalogue of CCS/New Horizons
subtitle: The obstacles of saving old source code
date: 2018-03-04
tags: ["source", "preserve", "kryoflux", "amiga", "ccs", "c", "Assembler"]
---

The other day a package arrived at my home. It was quite heavy, and inside it was a box of old floppy disks that had been lying in storage for many years. Half of the disks were marked with source, and they were supposed to contain the collected backups of Central Coast Software, New Horizons and finally Wci. By the current copyright owner I had been given the permission to release the code as open source - if I could manage to read the disks and find the source code.

The first thing I did was to hook up my Kryoflux (a device for preserving disks, even badly damaged). The second thing I did was to find some Isopropanol, and with the aid of an cleaning floppy clean out the old 3,5 drive I was planning to use for the babackups. And then I started to read them, putting the disks that wouldn't read in a pile of their own, and of course, focusing on the disks tagged with "sources" first. 

After the first round I ended up with about 14 disks that were either full of errors, or that I could only guess the format of.
(For the Kryoflux to know what to rip if not reading STREAM-format, you have to tell it the format). So I had to do some detective work. For the few disks that had read errors, I just cleaned the drive and re-read the disks and sectors until I had all data from. It took a while to get correct data from them.

For the mystery disks, I had to try and guess. It turned they were in the following formats: Amiga FFS HD. Mac 400, Mac 800, MacHD, and finally a set of Quarterbacked (an old amigaspecific backup tool) tools. 

When I all the data I had sort the source code in sets - there were a few revisions, and I wouldn't have the time to try to build anything. I arranged it so that every archive would have a few revisions. 

Sadly, there were some of the last versions missing, for example Quarterback only had it's source for up until 6.1, though there was a 7.31 version released. Still it's fantastic that any source code at all emerged. 

Well, all you need to save code is Kryoflux, a detective mindset, and a lot of patience!

The source code archives will soon be released under GPL, I'm just waiting for the current copyright owner to have a look at the readme.

Fun fact - the assembler source code to both a former mac and a pc word processor was included. I'll get them out too, in due time. 

---
title: Computers And Chaos by Conrad Bessant
date: 2018-05-16
tags: ["bessant", "books", "book", "creative commons"]
---

Another day, another book permission given. Conrad Bessant wrote "Computers and Chaos" back in 93 for the Amiga and Atari ST, and has given us the permission to release it under Creative Commons. The book is about writing fractals and fractal landscapes on the amiga and atari. We are so very glad for that, and look forward to giving it the PDF and Asciidoc treatment. However, our work queue is already filled for a while, as we plan to finish two of Overaas books, with some more source code releases in between, but hopefully it will be available after the summer.

Again, a big thanks to Conrad Bessant, and as always, support the authors more [recent works](https://www.amazon.com/s/ref=dp_byline_sr_book_1?ie=UTF8&text=Conrad+Bessant&search-alias=books&field-author=Conrad+Bessant&sort=relevancerank) if it is in your field of interest.

<img src="/img/computer-chaos-book.jpg" width="500" />


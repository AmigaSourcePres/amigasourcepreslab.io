---
title: How to convert PageMaker 2 and 4 files to Postscript
date: 2018-04-02
tags: ["pagemaker", "vmac", "postscript", "mac"]
---

I had a few manual files in an unknown format, and without any extensions that I wanted to read and convert. I was given some hints to as what they were by looking at them in a hexeditor - PM 4.2 it said. Hmmm - a discussion with a fellow retro interested contact gave that he thought they were in PageMaker format. But he had been unsuccessful in opening them. As I never, ever, had used an Apple OS from before maybe 2006, (and still don't do if I can avoid it), I had some read up to do. First, about PageMaker - PageMakers last version was 7, and it could run on different operating systems. It is the ancestor to InDesign. 

Ah - easy, I'll just get a recent trial version of InDesign and convert them! After some trial and error I learnt that PageMakers fileformat was a special kind of guy - the kind of guy that comes to your house, knocks you down, steal your car keys and only gives them back after a ransom is paid. PageMaker could only open files on the version above it, and they had stay on the same platform. Let us say you had PageMaker 2 files but PageMaker 4 installed. Than you first would have to open them and save them in version 3 before going to 4 (and only on the same platform). What about opening up these old files in a newer version of InDesign? [No](https://helpx.adobe.com/creative-suite/kb/open-pagemaker-files-indesign.html).

After installing vMac with system 7,  and finding suitable programs for importing, exporting I was ready. Ready to convert. But, PageMaker 4.2 could not open the files. I did not know about how terrible system 7 handles filemeta data information, so that led me to believe that the files were corrupted (Later I learnt they probably only had the wrong Creator Type). I did, after some search, find another source for the same files, that came straight from a mac disk image, and with more of the file metadata correct. Now, I could finally read some of them. After some more research I found the program Creator Changer. There I could finally identify that the files were created by [ALD4 and ALD2](https://discussions.apple.com/thread/975842), that is, PageMaker 2 and 4 formats. Great! Off to find PageMaker 3 (to be able to open the PageMaker 2 files in version 4 by converting them in PageMaker 3). Now, Why did I want to open even the old files in PageMaker 4.x you might ask. Because PageMaker 4 was the first version that gave me the possibility to also also print as postscript. 

So, to make this quite frustrating adventure shorter for you, I have prepared a vMac-image, with the few tools you need and PageMaker 3 and 4 on it, ready to go.
The tools are:

 * PageMaker 3 & 4
 * Creator Changer - for peeking at the file type
 * ExportFi & ImportFi - for being able to import and export files from vMac
 * MiniUnzip - Unzip zip files you might have imported  

<img src="/img/vmac_programs.png" width="600" />

You can read more about the tools [here](http://www.gryphel.com/c/minivmac/extras/index.html).
The image is based on the works from [Mini vMac Applications](https://sites.google.com/site/minivmacapplicationsv6/home) and also contains Stuff-it and other tools.
[Get the vMac image here (Linux, Mac)](https://mega.nz/#!WQIG3YYD!gytcnEsClFlyOt9y_iOJUPQmTzcBKS1vhNEqupxMK7I)

So, start up vmac, convert your PageMaker-files to PostScript and meet the future.

From what I know, all software in the vMac image is either free software, shareware, freely distributable or abandonware. If someone says otherwise and can prove that they are the copyright holders, let me know and I'll remove the image faster than you can say - "**I hate old PageMaker-files and they must be converted so that future generations wont go insane**", ten times in a row.

 * [vMac setup help](https://lazyretroist.wordpress.com/category/macintosh-plus/)
 * [vMac official site](http://www.gryphel.com/c/minivmac/index.html)
 * [Machintosh Repository](https://www.macintoshrepository.org/)
 * [Vintage Mac Repository](http://www.macfixer.com/vintage-software/)




Printing to PostScript in PageMaker 4

<img src="/img/pm_print_1.png" width="600" />
<img src="/img/pm_print_2.png" width="600" />
<img src="/img/pm_print_3.png" width="600" />
<img src="/img/pm_print_4.png" width="600" />

UPDATE: A retro contact of mine notified me that he had had found some artifacts by doing the above, and tried another way:

> This old Adobe PDF Writer (http://macintoshgarden.org/apps/adobe-acrobat-pdfwriter-30) creates a PDF file directly from a Print command - it's the print-to-file utility I'd been hoping to find. I used it in the Mini vMac environment you shared and was able to print the PageMaker 3 files directly to PDF, thus avoiding having to convert them to PageMaker 4 and the layout errors the format conversion introduced.

> I tried doing the same for the PageMaker 4 files, but it kept crashing. Maybe the Mini vMac environment didn't have enough resources? I think it's only a 68000 Mac with 4MB of RAM. So I tried it under my MacOS 8.1 environment under SheepShaver and it worked! (PageMaker 3 has a bug/compatibility problem that prevents it from printing on MacOS 7.6 and later, otherwise I would have done both the PM3 and PM4 conversions in my SheepShaver environment.)

> From what I can tell, the PDF Writer differs from the regular Adobe Acrobat/Distiller of the early 1990s in that it creates PDFs directly. Acrobat/Distiller creates PostScript first, and then converts that to PDF. That wasn't an option because of the earlier issue I mentioned about graphics getting lost in the PostScript conversion. That problem now appears to be solved!


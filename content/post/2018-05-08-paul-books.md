---
title: Paul Overaa's Amiga books
date: 2018-05-08
tags: ["overaa", "books", "book", "creative commons"]
---

There were many [books](http://wiki.classicamiga.com/Amiga_Reference_Books) released for the Amiga during its peak. If one searches around, most of them can be found on the internet, in various scanned formats. Even if most of their original authors or copyright owners would give their permission to release them under something like Creative Commons or even Public Domain if just asked, they most likely, have not been asked. And we like legally correct things and to have the authors permission for projects released on this site. It strengthens the Free licenses, makes it perfectly clear what can be done regarding derivative works, and it hopefully inspires more releases of old computing treasures.

Paul Overaa is an excellent technical writer, with a good knowledge of both the technical aspects of a computer, and with the skills to write in an interesting way for beginners and more experienced users alike. He was quite active during the Amiga years, and wrote the following books:

* Total Amiga Assembler  - Oct 1995 - 512 pages - ISBN 1873308574
* Mastering Amiga Programming Secrets - Feb 1995 - 368 pages - ISBN 1873308337
* Mastering Amiga Systems  - April 1992 - 398 pages - ISBN 187330806X
* Mastering Amiga Arexx - April 1993 - 336 pages - ISBN 1873308132
* Mastering Amiga Assembler - Nov 1992 - 416 pages - ISBN 1873308116
* Insider Guide Series: Amiga Assembler - Oct 1994 - 256 pages - ISBN 1873308272
* Insider Guide Series: Amiga Disks and Drives  - March 1994 - 256 pages - ISBN 1873308345
* Program Design Techniques for the Amiga - 1991


Therefore it is such a great thing that he gave us the permission to preserve/release the books under a Creative Commons license. We will try to offer good quality scans, online versions and asciidoc versions. We will also try to hunt down the disks that came with the books. Again, a big thanks to Mr Paul Overaa for making this happen.

Note: this is a project that will take some time to complete. Stay tuned as we add these (and other books) during the year.

Please also support the original and active author by buying one of his more current fiction [books](https://www.amazon.co.uk/Books-Paul-Overaa/s?ie=UTF8&page=1&rh=n%3A266239%2Cp_27%3APaul%20Overaa).

<img src="/img/overaa-books.jpg" width="500" />


---
title: Quarterback, DesignWorks and more
subtitle: Finally
date: 2018-03-18
tags: ["source", "designworks", "amiga", "ccs", "quarterback", "Assembler"]
---

The released mentioned in the longer [last post](/post/2018-04-03-preserving-code), the code for a batch of classics like Quarterback and DesignWorks, have now been released under GPL. Find them in our [Git-repos](/page/sourcecode/), and soon on aminet. Enjoy!



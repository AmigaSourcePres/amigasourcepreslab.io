---
title: The hunt for Benchmark Modula-2
date: 2018-06-29
tags: ["modula2", "Benchmark"]
---

It was with great preservation joy I received 16 old disks in the mail the other day, Containing some rare stuff regarding Modula-2, more precisely the Benchmark Modula-2 compiler environment, with updates and extras. As preservation if very much a detective work, it is always very rewarding when something interesting and concrete is found and preserved.

So this little adventure all started when I got permission from Michal Todorovic and Kailash Ambwani to relase the former Gold Disk applications File and Pro Calc as Public Domain (CC0). They were both written in Modula-2, and before releasing them, it would be interesting and fair to see if they were buildable, right out of the box. Now, I am a coder, but I have never, ever coded a line of Modula-2 in my life, so this also gave me the chance too see how it was done, and how Modula-2 worked. Curiosity killed the cat and all that you know. After trying a few Modula-2 compilers for the Amiga, with no success, a post from Mark Wickens led me on the right track - it looked like the code could have been originally built with Benchmark Modula-2. Mark has his own [story](https://www.wickensonline.co.uk/rc2012sc/2014/07/01/introduction/) about how he hunted down Benchmark Modula-2, as this wasn't available even on abandonware sites, and not findable as an original on ebay etc. He had done a fine job of scanning the manual, and had the first released version available for download. Sadly, one disk was missing fron the set, and all demo example could not be built, among them the classic [Gravity Wars by Ed Bartz](https://www.wickensonline.co.uk/rc2012sc/2014/07/22/gravity-wars/). And even more, the application File couldn't be built either. So, after more research, I did found out (in the amiga newsgroups archive) that the Benchmark Modula-2 history didn't stop after the first released Leon Frenkel version - it had been continued by Armadillo Computing, ie. Jim Olingers computer company in the 90's. Armadillo also made some tools for it, and updated it and continued development. Tom Breedan also got involved as a programmer to work on it, and, he continued working on the implementations, and a later PPC implementations of a Modula-2 compiler long after the highlights of the Amiga days were over.

<img src="/img/benchmarked.png" width="500" />

Anyway, After some more research I found Jim on Linkedin and got in contact - and he told me a bit about the Armadillo Computing-days, Benchmark Modula-2, and was positive to see it released under an Open License. However, he didn't have any physical items or code left, as it all had been handed over to Tom Breeden. So, basically, if Tom would be ok with releasing anything, Jim was fine with it too. So, the quest for Tom started -I tried the email given to Tom on his [Aglet page - check it btw, it is Modula-2 for AmigaNG](https://aglet.web.runbox.net/), but it hadn't been updated in a few years, and the email given there bounced. So, that's were the quest ended I thought. Then, by pure luck, I found his initials on an Amiga forum, with a fairly recent login date. Could it be the correct Tom? I messaged him, and then it was just a matter of waiting. Would he have any items left? Would he like the idea of releasing it as open source for preservation if so. Would he, if nothing else, have access to very rare imagelib-library, so that I could build File, Pro Calc and Galaxy Wars easily?

<img src="/img/benchmarkdisks.jpeg" width="500" />

And after some time I got an answer.... And Tom was all for the idea! And, after some digging in the archives Tom did find most of the important disks, and agreed to send them overseas for preservation as he didn't have any hardware left to read them with. They had been well stored, and was of high quality so my Kryoflux device didn't complain about any bad sectors at all. Also he found source code backups, which he is currently looking into, so we all hope for more Modula-2 goodies becoming preserved and available. As of now you can enjoy the various disk-images and the manual. I will most likely offer an Asciidoctor-version of the manual in the gitlab repo in the future. Have fun, an try some of the demo examples.

This was another exiting preservation journey, that is not over yet - thanks to all involved for being positive and helping to make it happen.

[Repository](https://gitlab.com/amigasourcecodepreservation/benchmark-modula-2)

PS. If you have the original BM2 box, please consider selling it or donating it. I would very much like to scan it.
PS. More about the Pro Calc and File-projects in another post, I'll be preparing them for release soon.



---
title: Slaygon
date: 2018-07-27
comments: true
---

<body style="background: #E1B00F; color: white;">


Slaygon is a rpg dungeon crawler created in 1988. 
Published by Microdeal and created by _John Conley_ and _James Oxley_ for the Atari, and later ported to Amiga by Timothy M. Purves.

<img src="/img/microdeal/slaygon/slaygon_box_front_amiga.jpeg" width="400" />


## Development

_We created Slaygon because most RPG style maze games at the time were lacking. You could not save your game without backtracking out of a dungeon and you had to use graph paper to map the dungeon. We resolved these and many other issues and Slaygon was actually very ahead of its time in many ways. I am still very proud of our accomplishment. Later we developed “Day of the Viper” when technology caught up with our ambitions and it was a much better game._ - John Conley

### Box Description

You have developed the ultimate infiltration device: The Slaygon. The Slaygon is the most sophisticated military robot ever created- conventional weapons have absolutely no effect on it! Controlled from within by one highly trained specialist, Slaygon possesses the strength of a hundred men, armory and weapons of a small tank, and the intelligence of its operator.

You and your remarkable machine are needed by the government to help uphold peace and justice in the world. The situation is critical and escalating. You immediately join the United Defense Force.

A top secret dispatch orders you to destroy the Cybordynamics Laboratory facility at all costs. They are working on a strain of toxic virus capable of annihilating all human life and allow them to capture the world. If you can disable their main computer, their facility will explode and their sinister plans will be thwarted.

You have managed to obtain a forged low security pass, but once inside your on your own. The location of the strategic targets are unknown. You must maneuver around the four different types of security robots that guard the complex and covertly wind through 5 miles of hallways and 500 rooms in this ¼ mile square complex. There are five security levels: if you can find a higher level keycard it will allow you to pass through lower level doors.

A special cloaking device makes Slaygon invisible to all security guards except the Base Commander. The Short Range Scanner allows the Slaygon to identify and notify you of immediate danger. Your shields will absorb destructive energy, but your energy level will decrease rapidly when they are on. If your energy supply is depleted, Slaygon will cease to function and your mission will fail.

There are over 3 dozen objects you can pick up that may help you. Some objects must be used, others carried. Certain objects will activate equipment in special rooms, while others may not. You carry Field or Mine Deactivators and D-Ionizer Rods for a safe passage through force fields, Proton Miners, and the dreaded Ionizer Beams. The Slaygon has storage room for 8 objects, but each object carried uses precious energy.

You fortunately have an energy level indicator, a Directional Indicator, a Map View of your movements from the top that is continually updated as you travel, and the Slaygon's 3-D Front View. The Message Display Screen allows Slaygon's on board computer to communicate with you.

Activate the Slaygon and prepare for the most important mission in the history of humanity. 

<img src="/img/microdeal/slaygon/slaygon_box_back_amiga.jpeg" width="400" />

## Open Source?

Like most other games from that period this game has been available (without permission) at various sites for many years, but in 2018 it was clearly given permission by the authors to be released under an open license. This release is part an ongoing effort to release the old Microdeal/Michtron titles for legal preservation.

Sadly, any source code for the game *might* have been lost in time, so all we can offer at the moment is the image files for usage in an emulator. However, the authors is looking through their archives. Also, we are in contact with the Amiga porter of the game.

## License

The game is currently released under an non-standard license, but were are in contact with John, suggesting a more standard non-commercial Creative Commons license.


_John Conley and Jim Oxley, authors and owners of the rights to the video game Slaygon confidently give our permission for you (amiga source code preservation) to continue making our game open-source by granting a limited license to Slaygon’s source code, as well as its graphics, music and sound files. Our only rules and conditions_

* All original rights for Slaygon still remain with the original authors
* Use of Slaygon’s graphics, music and sound files should not be done without our prior consent. 
* Give equal credit to John Conley and James Oxley as Slaygon’s creators. 
* The game, Slaygon, may not be released in any format on any platform for monetary gain. 
* Copy us on any future correspondence related to our game. 
* Keep in touch!


## Notes

_If we can help any new programmers and help to preserve what could easily be forgotten then what you are doing is aa good thing._ - John Conley

*Long live the users!* - John

## Help!!

- Are you a former Microdeal/Michtron contact or worker and happen have an old backup of the source code for any of the platforms? Please, help us and send it to our email. It is totally fine to send it anonymously.

## Many thanks to 

John Symes for being positive about the project and giving permission, The Dorsman Family for letting the Michtron rights revert to their authors, John Conley and James Oxley for being supportive of releasing the game and for creating a game ahead of its time.

## Downloads

### Diskimages

[Amiga & Atari](https://gitlab.com/amigasourcecodepreservation/slaygon/tree/master/release)


### Documentation

Manual in Asciidoc - Todo

[Amiga Manual (pdf)](https://gitlab.com/amigasourcecodepreservation/slaygon/blob/master/doc/slaygon_amiga.pdf)


### Box Scans

[A list here](https://gitlab.com/amigasourcecodepreservation/slaygon/tree/master/image) 

## References & More details

[Hall of light (Amiga)](http://hol.abime.net/1974)

[Atari Mania](http://www.atarimania.com/game-atari-st-slaygon_11247.html)



</body>

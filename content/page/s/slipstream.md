---
title: Slipstream
date: 2018-07-26
comments: true
---

<body style="background:#181818; color: white;">


Slipstream is a scrolling arcade game in the style of space harrier. Published by Microdeal in 1989 and created by _Ian Potts_ for the Amiga.

<img src="/img/microdeal/slipstream/slipstream_box_front.jpg" width="400" />


## Development

SlipStream was written in about 3 months during the autumn of 1988. Written in 68k assembler, It was Ian's first major Amiga game after several Commodore 64 ones. 

### Box Description

There are 9 Streams of the SlipStream which have been overtaken by an unknown alien race who have ridden the planet with various defense space craft. Each of these craft are powered by a crystal which lies at the end of each Stream.

Mission:
You must fly through each Stream destroying the power crystal at the end of each one in order to deactivate the defense craft - hence liberating the 9 Streams of the SlipStream.

After disembarkment from your mothership, you must destroy as much of the landscape and as many of the defense craft as possible.

Priority order- You must destroy the power crystal at the end of each Stream if your mission is to succeed. No two Streams are alike. Featured in the 9 streams are towers, tunnels, arches, H-Squares and even solid walls set to block your entrance unless you can blast them to smithereens!! The secret of success is learning the layout of each individual Stream and conquering the various obstacles.

It's not impossible, it can be done and you are considered to be brave and skillful enough to tackle the death defying task that lies within the SlipStream.

Fast, solid 3D scrolling in a full screen display. Fast, solid 3D scrolling in a full screen display. Stunning backdrops that make full use of the NTSC or PAL displays. Convincing stereo sound effects. Weather conditions option. Sound on/off option. Random mission order option - designed to make the same screen new and exciting every time!! Full high-score table. 

<img src="/img/microdeal/slipstream/slipstream_box_back.jpg" width="400" />

## Open Source?

Like most other games from that period this game has been available at various sites for many years, but in 2018 it was clearly given permission by Ian Potts to be released under an open source license, the MIT License. This is an ongoing effort to release the old Microdeal/Michtron-titles for legal preservation.

Sadly, any source code for the game *might* have been lost in time, so all we can offer at the moment is the image files for usage in an emulator. However, Ian is looking through his archives.

## Trivia

*A unique aspect of the game was the use of a full screen overscan display (no black borders around the edge) which I thought looked great....but alas there was a downside. The graphics chip used the 'downtime' of rendering the black borders to process faster... Whereas if you extended the screen size to use overscan it Looked better but you lost performance... So SlipStream might have had a faster frame rate if hadn't been quite so ambitious.... But pushing technical boundaries was always what I enjoyed (my previous C64 game, Spartacus, had the largest sprites of any C64 game by using the standard 8 but repositioning them during vertical scans further down the screen immediately under the first 8. In this I had a total of around 32 sprites on screen. I don't know of any C64 title that got the timing just right like that to achieve a similar effect.*

*SlipStream had a sequel... But it was never published. I spent about 18 months developing it. It was called The Jupiter Run, it was a vast Space Opera, and at the time I thought it was great... But sadly no publisher I approached wanted to take it on as they all said that space games were out of fashion!*

*So I changed course and began work on a word processor... In May 1991 that was launched as Digita Wordworth, soon established itself as one of the market leaders on the Amiga, and the rest is history.* - Ian Potts

## Help!!

- Are you a former Microdeal/Michtron contact or worker and happen have an old backup of the source code for any of the platforms? Please, help us and send it to our email. It is totally fine to send it anonymously.
- Do you want to resource the game to restore the source code. Go ahead, we encourage you, don't forget to notify us so we can preserve it.

## Many thanks to 

John Symes for being positive about the project and giving permission, Ian Potts for being supportive of releasing the game and for creating this cool game.

## Downloads

### Diskimages

[Amiga](https://gitlab.com/amigasourcecodepreservation/slipstream/tree/master/release)


### Documentation

[Manual in Asciidoc](https://gitlab.com/amigasourcecodepreservation/tanglewood/blob/master/doc/tanglewood_manual.adoc)

[Amiga Manual (pdf)](https://gitlab.com/amigasourcecodepreservation/tanglewood/raw/master/doc/tanglewood_manual_amiga.pdf)


### Box Scans

[A list here](https://gitlab.com/amigasourcecodepreservation/slipstream/tree/master/image) 

## References & More details

[Hall of light (Amiga)](http://hol.abime.net/1982)




</body>

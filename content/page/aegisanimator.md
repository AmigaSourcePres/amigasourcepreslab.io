---
title: Aegis Animator
date: 2018-06-28
comments: true
---

<body style="color: black;">

Aegis Animator is an early paint and animation package, written by Jim Kent and published by Aegis Development. From the packaging: "Aegis Animator will turn your Amiga computer into a full function animation workstation capable of creating any type of moving computer/video storyboard. Designed for use by the graphics professional, artist, video post production company, or the Amiga hobbiest, Aegis Animator makes use of a unique process called "tweening" which allows movement and change within each segment of the animation. "

<img src="/img/aegisanimator_box_front_amiga.jpg" width="400" />


## Development

Aegis Animator was written by Jim Kent and published by Aegis Development in 1985. The first version was released on the Amiga (1.x) and the following versions (2,x) was released on Atari ST only. After the last released version, the program was developed a bit further, until Jim went on to other projects. 

Thorsten Otto spent some time with the code and got it to build for Atari with Megamax C-compiler, see the file build.txt in the [git-repo](https://gitlab.com/amigasourcecodepreservation/aegisanimator)

## Open Source?

Aegis Animator was released under MIT in 2018 after Jim Kent gave permission for the source code. The code is the only known surving piece of code for Aegis, and it is a late version of the Atari ST code. However, there seems to be an placeholder for a missing libamiga in the code, so how much it would take to re-write the lost Amiga parts - well we encourage you to try!!

## Help!!

- Do you want to resource the Amiga part or make it build with a modern compiler?. Go ahead, we encourage you, just don't forget to notify us so we can put it up on our gitlab.

## Trivia

"This may be the Atari ST version, or maybe the changes are ifdefed.  Could even be the version that I developed but never released. At any rate it's something and I think it's all I got.  For the later versions at least I used the Aztec compiler.  I liked that one so much I helped them port it to the Atari ST." - Jim Kent

For more trivia, see the "The story of Aegis Animator and Cyber Paint"under references.

## Many thanks to 

Jim Kent for giving his permission to release the code under MIT, Thorsten Otto for making an effort of building it again, and Olof Barthel for taking a peek at the code.

## Downloads

### Diskimages

[Amiga](https://gitlab.com/amigasourcecodepreservation/aegisanimator/tree/master/release)

[Atari ST](https://gitlab.com/amigasourcecodepreservation/aegisanimator/tree/master/release)

### Documentation

[Manual in Asciidoc] Not Yet

[Inside Aegis Animator (pdf)](https://gitlab.com/amigasourcecodepreservation/aegisanimator/raw/master/doc/Aegis_Inside_Aegis_Animator_A_User's_Guide.pdf)


### Box Scans

[A list here](https://gitlab.com/amigasourcecodepreservation/aegisanimator/tree/master/image) 

## References & More details

[The story of Aegis Animator and Cyber Paint](http://doudoroff.com/atari/cyberpaint.html)

[Elis software encyclopedia (Amiga)](http://www.elisoftware.org/index.php/Aegis_Animator_(Amiga,_3_1/2%22_Disk)\_Aegis\_Development\_-\_1985\_USA,\_Canada\_Release)

[AtariMania (Atari)](http://www.atarimania.com/utility-atari-st-aegis-animator_23234.html)


</body>

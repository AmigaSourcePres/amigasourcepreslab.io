---
title: Microdeal Archives
date: 2018-07-20
comments: true
---

An ongoing effort to archive, preserve and open source the works that was published under the Microdeal and Michtron-label. Focus is on the Amiga, but there will be other platforms too.

## History

This page was started after contacting John Symes, founder of Microdeal. He was supportive of the project of preserving the Microdeal/Michtron titles, and also mentioned that they only acted as publishers, and that the works belonged to their different authors. Their _publishing_ rights were sold to HiSoft after they closed doors. 

That initiated an ongoing search for the different authors involved (in the Amiga production mainly, but often that is tied to other platforms too) and asking for their permissions, and also asking if they have any of their old works preserved.

For more information about Microdeal and their ties to Michtron, see their entry at [Wikipedia](https://en.wikipedia.org/wiki/Microdeal). As for their sister company in the U.S, Michtron, it had some in-house development, and was sold further after closing doors. After reaching out to the current Michtron-rights holders, The Dorsman family, they were glad to help!

> We, the relatives of Mr James A. Dorsman, owner of former Creative Computer Corps. lets any eventual exisiting copyright for the MichTron titles return to their respective authors with the intent of allowing them to open source, or release their works to public domain.
> If you're in touch with the original authors of Airball, Time Bandit, TangleWood, etc. -- it would be great to get those sources and games into the project!  It would be great to even play some of them again.

If the source code is lost, we still would like to preserve and release the disc images and other memorabila, under MIT (or something else, it is up the author), which allows a liberal usage. 

And finally - yes, all titles have been available for many years from different abandonware sites, but this is an effort to also preserve and make the usage **fully legal and correct, respecting the authors works** and letting anyone interested in 
seeing how the programs were constructed back in that era get a peek into the essentials, and also letting anyone interested in retro tech even rebuild and fix that irritating bug, create that new level and so on.


## Program list

 <table>
  <thead><tr><th>Title</th><th></th><th></th></tr></thead>

<tbody>

 <tr>
      <td>Slaygon</td>
      <td> <div> <img src="/img/microdeal/slaygon/startscreen.png" style="min-width: 30px; width: 200px;" /> </div></td>
      <td>
         <div>
                <div>A classic rpg dungeon crawler</div>
               <div>[The Slaygon Page]({{< ref "page/s/slaygon.md" >}})</div>
            </http:>
         </div>
      </td>

 <tr>
 <tr>
      <td>Slipstream</td>
      <td> <div> <img src="/img/microdeal/slipstream/startscreen.png" style="min-width: 30px; width: 200px;" /> </div></td>
      <td>
         <div>
                <div>A classic arcade game/3d scroller</div>
               <div>[The Slipstream Page]({{< ref "page/s/slipstream.md" >}})</div>
            </http:>
         </div>
      </td>

 <tr>
      <td>Quartet</td>
      <td> <div> <img src="/img/microdeal/quartet/quartet.png" style="min-width: 30px; width: 200px;" /> </div></td>
      <td>
         <div>
                <div>A classic music program for Amiga and Atari</div>
               <div>[The Quartet Page]({{< ref "page/q/quartet.md" >}})</div>
            </http:>
         </div>
      </td>
   </tr>

 <tr>
      <td>Tanglewood</td>
      <td> <div> <img src="/img/microdeal/tanglewood/startscreen_amiga.png" style="min-width: 30px; width: 200px;" /> </div></td>
      <td>
         <div>
                <div>A puzzle/adventure game</div>
               <div>[The Tanglewood Page]({{< ref "page/t/tanglewood.md" >}})</div>
            </http:>
         </div>
      </td>
   </tr>

 <tr>
      <td>Time Bandit</td>
      <td> <div> <img src="/img/microdeal/timebandit/startscreen_amiga.png" style="min-width: 30px; width: 200px;" /> </div></td>
      <td>
         <div>
                <div>An adventure game</div>
               <div>[The Time Bandit Page]({{< ref "page/t/timebandit.adoc" >}})</div>
            </http:>
         </div>
      </td>
   </tr>
   <tr>
   <td>Turbo Trax</td>
      <td> <div> <img src="/img/microdeal/turbotrax/startscreen.png" style="min-width: 30px; width: 200px;" /> </div></td>
      <td>
         <div>
                <div>A racing game</div>
               <div>[The Turbo Trax Page]({{< ref "page/t/turbotrax.md" >}})</div>
            </http:>
         </div>
      </td>
   </tr>

</tbody>

</table>
## Ongoing permission effort

Author              | Title         | Role                | Permission
--------------------|---------------|---------------------|---------------------------|
Ian Murray-Wattson  | Tanglewood    | Programmer          | Yes                       |
Ian Murray-Watson   | Turbo Trax    | Programmer          | Yes                       |
Pete Lyon           | Airball       | Graphics            | Yes                       |
Pete Lyon           | Soccer        | Graphics            | Yes                       |
Pete Lyon           | Airball       | Graphics            | Yes                       |
Pete Lyon           | Fright Night  | Graphics            | Yes                       |
Pete Lyon           | Gold Runner   | Graphics            | Yes                       |
Pete Lyon           | Karate Kid 2  | Graphics            | Yes                       |
Pete Lyon           | Leatherneck   | Graphics            | Yes                       |
Pete Lyon           | Tanglewood    | Graphics            | Yes                       |
Harry Lafnear       | Major Motion  | Graphics            | Yes                       |
Harry Lafnear       | Time Bandit   | Graphics            | Yes                       |
John Symes          | *             | Microdeal Founder   | Yes                       |
Bill Dunlevy        | Time Bandit   | Programmer          | Maybe                     |
Timothy Purves   | Goldrunner 2  | Programmer (Amiga)  | Positive                  |
Timothy Purves   | Major Motion  | Programmer (Amiga)  | Positive                  |
Timothy Purves   | Slaygon       | Programmer (Amiga)  | Positive                  |
Timothy Purves   | Talespin      | Programmer (Amiga)  | Positive                  |
Timothy Purves   | Tetra Quest   | Programmer (Amiga)  | Positive                  |
Timothy Purves   | Time Bandit   | Programmer (Amiga)  | Positive                  |
Timothy Purves   | Tanglewood    | Programmer (Amiga)  | Positive                  |
John Conley         | Slaygon       | Graphics & Music | Yes                     |
James Oxley         | Slaygon       | Programmer          | Yes                     |
Chris Kew           | Jupiter Probe | Graphics            | Pending                   |
Chris Kew           | Tetra Quest   | Graphics            | Pending                   |
Paul  Shields       | Airball       | Music               | Can't locate              |
Paul  Shields       | Soccer        | Music               | Can't locate              |
Paul  Shields       | Goldrunner 2  | Music               | Can't locate              |
Paul  Shields       | Jug           | Music               | Can't locate              |
Paul  Shields       | Tetra Quest   | Music               | Can't locate              |
Ed Scio             | Airball       | Programmer          | Pending                   |
Ed Scio             | Soccer        | Programmer          | Pending                   |
Paul Hunter         | Airball       | Music               | Pending                   |
Paul Hunter         | Soccer        | Music Code          | Pending                   |
Paul Hunter         | Jug           | Programmer          | Pending                   |
Mark Heaton         | Talespin      | Programmer          | Pending                   |
Rudyard Heaton      | Grail         | Design              | Pending                   |
Kevin Cowtan        | Quartet       | Programmer          | Yes                       |
Rob Povey           | Quartet       | Programmer          | Yes                       |
Dan Lennard         | Quartet       | Programmer (Amiga)  | Yes                       |
Dorsman family (CCC)| *             | Michtron righs holder | Yes                     | 
Paul Frewin         | Tetra Quest   | Programmer (Atari ST) | Yes                     | 
Ian Potts           | Slipstream    | *        | Yes                     |



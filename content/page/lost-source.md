---
title: Lost Source Code
subtitle: He's dead, Jim
date: 2018-04-30
comments: true
---

* Source lost = It's gone
* Not found = No response or author/copyright owner not found at all. Help us here.
* Declined = Copyright owner contacted, but did not want to open source their code.



Title                     | Author                        | Status                                          |
----------------------------|-------------------------------|-------------------------------------------------|
[1497](http://aminet.net/package/game/2play/1497)                   | Jan Hagqvist                  | Declined        |
[3DEngine](http://aminet.net/package/dev/amos/3DEngine.lha)             | Pedro Gil (Balrog Soft)       | Source lost     |
[3D Space Battle](http://aminet.net/package/game/2play/3DSpaceBattle)          | Giuseppe or Paolo Perniola    | Source lost     |
[adfm](http://aminet.net/package/misc/emu/adfm_v2.lha)              | Krzysztof Cmok                | Not found       |
[Aegis Animator](http://www.pcmuseum.ca/details.asp?id=36901&type=software)   | Jim Kent                      | Source lost     |
[All](http://aminet.net/package/util/arc/AII)                      | Paul Mclachlan                | Source lost     |
[AmiMasterGear](http://aminet.net/package/misc/emu/AmiMasterGear.lha)        | Gaelan Griffin                | Not found       |
[AmiRom64](http://aminet.net/package/misc/emu/AmiRom64.lha)             | Salim Gasmi                   | Source lost     |
[AmiToolBar](http://aminet.net/package/util/wb/AmiToolBar)                | Daniel Balster                | Source lost     |
[AmiVCSMaker](http://aminet.net/package/misc/emu/amiVCSMAKER)              | Boran Serkan (portacall)      | Source lost     |
[AmiZipRecover](http://aminet.net/package/util/misc/AmiZipRecover)           | Boran Serkan (portacall)      | Source lost     |
[Ampu](http://aminet.net/package/game/2play/ampu101)                | Riku Rakkola                  | Source lost     |
[anes](http://aminet.net/package/misc/emu/anes.lha)                 | Morgan Johannson              | Declined        |
[aPersonal](http://aminet.net/package/util/misc/aPersonal)               | Boran Serkan (portacall)      | Source lost     |
[ASp emu](http://aminet.net/package/misc/emu/ASpEmu)                   | Ian Greenway                  | Declined        |
[AssignManager](http://aminet.net/package/util/boot/AssignManager.lha)       | Matt Francis                  | Source lost     |
[ATB1213](http://aminet.net/package/util/wb/atb213)                    | Daniel Balster                | Source lost     |
[AtomicTetris](http://aminet.net/package/game/2play/AtomicTetris)           | Paul Mclachlan                | Source lost     |
[Black Belt Systems](http://www.datapipe-blackbeltsystems.com/amiga.html)                      | Black Belt Systems/Ben William| Declined (ltr?) | 
[Breakman](http://aminet.net/package/util/sys/breakman.lha)             | Krzysztof Cmok                | Not found       |
[CarCost](http://aminet.net/package/biz/dbase/carcosts)                | Rüdiger Dreier                | Source lost     |
[ChangeImg](http://aminet.net/package/util/wb/ChangeIMG)                 | Boran Serkan (portacall)      | Source lost     |
[ChipEm](http://aminet.net/package/misc/emu/ChipEm.lha)               | Pedro Gil (Balrog Soft)       | Source lost     |
[ConvertReko](http://aminet.net/package/gfx/conv/ConvertReko.lha)          | Deok-Min Yun (dmyun)          | Source lost     |
[Cur2ilbm](http://aminet.net/package/gfx/conv/cur2ilbm.lha)             | Deok-Min Yun (dmyun)          | Source lost     |
[CyberWB_EVD](http://aminet.net/package/misc/emu/CyberWB_EVD)              | Daniel Balster                | Source lost     |
[DefPubScreen](http://aminet.net/package/misc/fish/fish-0910.lha)           | Matt Francis                  | Source lost     |
[DevMan](http://aminet.net/package/util/cdity/devman14.lha)           | Eric Sauvageau                | Declined        |
[EasyInstaller](http://aminet.net/package/util/sys/easyInstaller.lha)        | Boran Serkan (portacall)      | Source lost     |
[eBook](http://aminet.net/package/comm/misc/eBook.lha)               | Boran Serkan (portacall)      | Source lost     |
[eFetch](http://aminet.net/package/comm/misc/eFetch_v1.lha)           | Boran Serkan (portacall)      | Source lost     |
[ExtractLGP](http://aminet.net/package/misc/emu/ExtractLGP.lha)           | Deok-Min Yun (dmyun)          | Source lost     |
[Fishlib](http://aminet.net/package/util/sys/flshlib)                  | Krzysztof Cmok                | Not found       |
[GPDMS](http://aminet.net/package/util/arc/GPDMS)                    | Giuseppe or Paolo Perniola    | Source lost     |
[GPJS](http://aminet.net/package/util/misc/GPJS)                    | Giuseppe or Paolo Perniola    | Source lost     |
[Graph2d](http://aminet.net/package/misc/math/graph2d320.lha)          | Kai Nickel                    | Source lost     |
[GUISgml](http://aminet.net/package/comm/www/GuiSGML)                  | David Trollope                | Source lost     |
[IBMConv](http://aminet.net/package/misc/emu/IBMConv)              | David Trollope                | Source lost     |
[iFlush](http://aminet.net/package/util/cli/iFlush.lha)               | Boran Serkan (portacall)      | Source lost     |
[IntuiCalcMUI](http://aminet.net/package/misc/math/IntuiCalcMUI11)          | Rüdiger Dreier                | Source lost     |
[Inv3D02](http://aminet.net/package/game/demo/Inv3D02)                 | Giuseppe or Paolo Perniola    | Source lost     |
[LottoGol](http://aminet.net/package/misc/misc/LottoGol)                | Giuseppe or Paolo Perniola    | Source lost     |
[LSLComp](http://aminet.net/package/dev/lang/LSLComp.lha)              | Pedro Gil (Balrog Soft)       | Source lost     |
[mac2ni](http://aminet.net/package/gfx/conv/mac2ni)                   | Daniel Balster                | Source lost     |
[MailArchiver](http://aminet.net/package/comm/mail/MailArchiv38_3.lha)      | Denis Gounelle                | Source lost     |
[MakePatPrefs](http://aminet.net/package/util/wb/MakePatPrefs)              | Daniel Balster                | Source lost     |
[MAMEGui](http://aminet.net/package/misc/emu/mamegui.lha)              | Stephen Sweeney               | Source lost     |
[MathPlot](http://aminet.net/package/misc/math/mathplot)                | Rüdiger Dreier                | Source lost     |
[mformation](http://aminet.net/package/disk/misc/mformat18a.lha)          | Eric Sauvageau                | Declined        |
[mkf](http://aminet.net/package/util/crypt/mKF.lha)                | Boran Serkan (portacall)      | Source lost     |
[mpegrip](http://aminet.net/package/gfx/misc/mpegrip.lha)              | Boran Serkan (portacall)      | Source lost     |
[MrMIDI](http://aminet.net/package/mus/midi/MrMIDI.lha)               | Deok-Min Yun (dmyun)          | Source lost     |
[MrMPEG](http://aminet.net/package/mus/play/MrMPEG.lha)               | Deok-Min Yun (dmyun)          | Source lost     |
[MToolLibrary](http://aminet.net/package/util/libs/MToolLibrary)            | Rüdiger Dreier                | Source lost     |
[NeoMAMEGUI](http://aminet.net/package/misc/emu/NeoMAMEGUI.lha)           | Stephen Sweeney               | Source lost     |
[NewIcons](http://aminet.net/package/util/wb/NewIcons46.lha)            | Eric Sauvageau                | Declined        |
[NewPatterns](http://aminet.net/package/gfx/misc/NewPatterns1.1)           | Daniel Balster                | Source lost     |
[nslookup](http://aminet.net/package/comm/tcp/nslookup.lha)             | Salim Gasmi                   | Source lost     |
[PathMan](http://aminet.net/package/util/wb/PathMan100.lha)            | Matt Francis                  | Source lost     |
[Plotter](http://aminet.net/package/misc/math/Plotter)                 | Rüdiger Dreier                | Source lost     |
[Poker](http://aminet.net/package/game/misc/Poker)                   | Giuseppe or Paolo Perniola    | Source lost     |
[QuickLib](http://aminet.net/package/dev/basic/QuickLib)                | Giuseppe or Paolo Perniola    | Source lost     |
[ReComp](http://aminet.net/package/util/pack/\_ReComp.lha)            | Boran Serkan (portacall)      | Source lost     |
[Rego](http://aminet.net/package/biz/dbase/Rego)                    | Paul Mclachlan                | Source lost     |
[Remover](http://aminet.net/package/util/misc/remover.lha)             | Almos Rajnai                  | Source lost     |
[RGBx](http://aminet.net/package/util/dtype/RGBx_DT.lha)            | Deok-Min Yun (dmyun)          | Source lost     |
[SetMan]( http://aminet.net/package/util/boot/SetMan21.lha)           | Eric Sauvageau                | Declined        |
[ShapeCr](http://aminet.net/package/dev/basic/ShapeCr)                 | Giuseppe or Paolo Perniola    | Source lost     |
[SheepCommand](http://aminet.net/package/game/shoot/SheepCommand.lha)       | Pedro Gil (Balrog Soft)       | Source lost     |
[SoftConfig](http://aminet.net/package/util/misc/SoftConfig_v36.lha)      | Denis Gounelle                | Source lost     |     
[SPCRecord](http://aminet.net/package/misc/emu/SPCRecord.lha)            | Gaelan Griffin                | Not found       |
[SpeedLoad](http://aminet.net/package/util/wb/SpeedLoad_V1.4.lha)        | Salim Gasmi                   | Source lost     |
[SpyVsSpy_3D](http://aminet.net/package/game/2play/SpyVsSpy_3D.lha)        | Pedro Gil (Balrog Soft)       | Source lost     |
[SuperEnalotto](http://aminet.net/package/misc/misc/SuperEnalotto)           | Giuseppe or Paolo Perniola    | Source lost     |
[Surfaceplot](http://aminet.net/package/misc/math/surfaceplot)             | Ole Bak-Jensen                | Source lost     |
[SysInspector](http://aminet.net/package/util/moni/SysInspector14.lha)      | Eric Sauvageau                | Declined        |
[Tank Squadron](http://aminet.net/package/game/strat/Tanx_Squadron.lha)      | Stephen Sweeney               | Source lost     |
[Tanxedit](http://aminet.net/package/game/edit/tanxedit.lha)            | Stephen Sweeney               | Source lost     |
[TDPrefs](http://aminet.net/package/disk/misc/TDPrfs10.lha)            | Eric Sauvageau                | Declined        |
[Totogol](http://aminet.net/package/misc/misc/Totogol)                 | Giuseppe or Paolo Perniola    | Source lost     |
[TransNib](http://aminet.net/package/misc/emu/TransNib100.lha)          | Matt Francis                  | Source lost     |
[uklottery](http://aminet.net/package/util/misc/uklottery2009_v11.lha)   | Boran Serkan (portacall)      | Source lost     |
[WinRes](http://aminet.net/package/misc/emu/WinRes.lha)               | Deok-Min Yun (dmyun)          | Source lost     |
[XBN](http://aminet.net/package/util/dtype/XBM_DT.lha)             | Deok-Min Yun (dmyun)          | Source lost     |
[xPAckGauge](http://aminet.net/package/util/dopus/xPackGauge)             | Daniel Balster                | Source lost     |
[XSA](http://aminet.net/package/util/pack/XSA.lha)                 | Deok-Min Yun (dmyun)          | Source lost     |
[YAAE](http://aminet.net/package/misc/emu/YAAE.lha)                 | Rikard Nordgren               | Source lost     |
[YAGAC](http://aminet.net/package/game/role/YAGAC.lha)               | Pedro Gil (Balrog Soft)       | Source lost     |
[ZipRecovery](http://aminet.net/package/util/misc/ZipRecoverV3.lha)        | Boran Serkan (portacall)      | Source lost     |
[ZXAM](http://aminet.net/package/misc/emu/ZXAM20b_060.lha)          | Toni Pomar                    | Source lost     |
[ZXAM](http://aminet.net/package/misc/emu/ZXAM20b.lha)              | Toni Pomar                    | Source lost     |
[ZX Spectrum](http://aminet.net/package/misc/emu/zxspectrum4.71)           | Jeroen J. Kwast               | Declined        |

---
title: About/Why?
subtitle: Why do we care?
comments: false
---

Dust. Mould. Magnetic loss. Age. Many dangers are out there. Destroying
your old amiga code. Code that was once was written for fun, for the urge of learning,
for commercial purposes, or just because. 

Now that code is part of our history, our computing history. And we want
to preserve it, so that future generations might have a glance of a part of
our computing evolution. But, we also enjoy the thought of current
and future hobbyists running code on their retro platforms,
and even being able to fix bugs..

We focus on finding authors that released their code on aminet, or former 
commercial ventures that folded, and ask them if they can find their code 
and kindly let us release it under an open license, so that we can preserve it on aminet/gitlab. 

We respect and understand that the old code might not be written in the modern way one would write it today, and
that the practices of how to write code might have changed in many ways. The coding style, or the language used is unimportant - We want to preserve as much as we can of certain period in computing history that affected a lot of us.
e.

### Are you a commercial project?

Certainly not. This is a pure preservation project, done for the love of the machine, source code in all it's forms, computing history and open source. It is done in our spare time. We would be richer if we spent that time consulting  or mining bitcoins instead, but we think it is more interesting to save code before it is gone. See the license choices we suggest further down.

### Do you have any plans to use the code commercially?

No. We have the plans to try to preserve more code and open source it. That's it. 

### Which licenses do you suggest?

For software we recommend any of the following:

* [MIT](https://choosealicense.com/licenses/mit/) - allows a very liberal use of the software.
* [GPLv3](https://choosealicense.com/licenses/gpl-3.0/) - allows a very liberal use of the software, but the user must also share any code that is changed or derived from the work
* [LGPLv3](https://choosealicense.com/licenses/lgpl-3.0/) -like GPLv3 but for libraries
* [Creative Commons BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) - liberal usage but explicitly disallows commercial usage. If possible, use GPLv3 instead for software. 

For books and other media:

* [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) - allows liberal usage.
* [Creative Commons BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) - liberal usage but explicitly disallows commercial usage.

For public domain:

* [Creative Commons CC0](https://creativecommons.org/share-your-work/public-domain/cc0/) - the works becomes public domain.

NOTE: In no way does open sourcing your work remove your copyright (That is only the case if you use Public Domain, i.e Creative Commons CC0).

For a good and simple overview of other licenses you can also see [Choose A License](https://choosealicense.com/).




### I created a project a long time ago, but my source code is gone. The binary is still intact.

You can still help this project. Was the binary released under a home made freeware license, or maybe none at all? Release your software under the MIT License, and it will have a liberal and respected license, but still be copyrighted to you.
This will make the project legally clear for the purpose of preservation and remove any uncertainty regarding usage or derivate works.

### I found some copyrighted code in one of the projects you released

Let us know as soon as possible. These things will happen now and then -  these are old projects, and every line of code can not be validated. We will try to resolve any such situation as fast as we can.

### Do you update the code?

Most of the times we get a bunch of files and the original programmer doesn't remember how it used to build. So, if we have time, we usually try to arrange it into a standard project structure, and try to build with some not too obscure tools. So for the initial commit, we try to avoid changing any source code. However, after that, the code is preserved, and from now on it is ok modernize the project in our humble opinions.  Any help here is much valued - help us make our projects buildable.  

### And books, what about them?

We love old technical books. But, they can be very hard to find in a good shape now-a-days. Sure, most of them books about the Amiga can be found in different places on the net, but we try to contact the copyright owner and ask them if they would allow us to release it under Creative Commons. That means that, the work, besides being preserved legally, also can shared, converted to various formats, and even forked if so wished.
For preservation purposes we like to keep a scanned copy of the original, and for the other benefits, our plan is to keep an asciidoc version.

### Project xxx is also available on aminet, or on site x, why do you have it here?
 
 Either it is a newer version, or a version with a license update. We try to host all dependencies for the released source code in our repos. That way, it should be easy to find everything needed to build a project. And, you will have versioning and other benefits in a git-repo.
 We love aminet, and intend to do upload everything we host there. We only need to fix this, that and... Our goal, which is impossible to reach 100%, would be that all depending components for a project also were open source.

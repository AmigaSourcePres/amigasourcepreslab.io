---
title: Tanglewood
date: 2018-06-06
comments: true
---

<body style="background:#54ab54; color: black;">


Tanglewood is a puzzle/adventure computer game produced by Microdeal in 1986. It was initially released for the Dragon 32 and TRS-80 Color Computer in early 1987 and later converted for the 16-Bit Atari ST and Amiga in 1988.

<img src="/img/microdeal/tanglewood/tanglewood_box_front_amiga.jpg" width="400" />


## Development
The game had originally been planned to be based on the children's TV series Willo the Wisp but this was dropped. It was designed and programmed by Ian Murray-Watson with graphics by Pete Lyon.

<img src="/img/microdeal/tanglewood/tanglewood_box_front_dragon_tandy.jpg" width="400" />

## Open Source?

The game has been available at various sites for many years, but in 2018 it was clearly given permission by all involved to released under an open source license, the MIT License, after asking permission from Ian Murray-Watson, Pete Lyon and former Microdeal publisher John Symes. This is an ongoing effort to release the Microdeal/Michtron-titles for legal preservation.

Sadly, any source code for the game has been lost in time, so all we can offer at the moment is the image files for usage in an emulator. 


## Help!!

- Are you a former Microdeal/Michtron contact or worker and happen have an old backup of the source code for any of the platforms? Please  help us and send it to our email. It is totally fine to send it anonymously.
- Do you want to resource the game to restore the source code. Go ahead, we encourage you, don't forget to notify us.

## Trivia

*I'm quite sure I don't have the old Dragon 32 tape, though I think that in many ways it was a better game - more fun. Of course,, it was a marvel of pruning. I think that by the time I had finished there were less than a dozen bytes available anywhere, including the system areas. I remember spending hours cutting every possible punctuation out of text, shortening words, brutally shortening messages and so on, just to get it all in. Sadly, by the time it came out the Dragon 32 was already on the way out.*

*The original for Tanglewood (as far as I know this is unknown) was a TV cartoon called 'Willo the Wisp'', which featured the voice of Kenneth Williams and a host of wonderful characters like Evil Edna, and Mavis the Fat Fairy. We used to watch it every week with the kids, and bought tapes of it for car journeys. It can still be found on the web.*

*I based all the characters and story line on this, but when the time came to get permission, they wanted some ridiculous percentage - like 40% - so I simply changed the story and the characters* - Ian Murray-Watson

## Many thanks to 

John Symes for being positive about the project and giving permission, Ian Murray-Watson and Pete Lyon for creating this wonderful game, and being permissive and supporting of preserving their work under liberal usage and license.

## Downloads

### Diskimages

[Amiga](https://gitlab.com/amigasourcecodepreservation/tanglewood/tree/master/release)

[Atari ST](https://gitlab.com/amigasourcecodepreservation/tanglewood/tree/master/release)

[Dragon32 & Tandy](https://gitlab.com/amigasourcecodepreservation/tanglewood/tree/master/release)

### Documentation

[Manual in Asciidoc](https://gitlab.com/amigasourcecodepreservation/tanglewood/blob/master/doc/tanglewood_manual.adoc)

[Amiga Manual (pdf)](https://gitlab.com/amigasourcecodepreservation/tanglewood/raw/master/doc/tanglewood_manual_amiga.pdf)

[Dragon 32 & Tandy Manual (pdf)](https://gitlab.com/amigasourcecodepreservation/tanglewood/raw/master/doc/tanglewood_manual_dragon_tandy.pdf)

[Atari ST Manual (pdf)](https://gitlab.com/amigasourcecodepreservation/tanglewood/raw/master/doc/tanglewood_manual_atarist.pdf)

### Box Scans

[A list here](https://gitlab.com/amigasourcecodepreservation/tanglewood/tree/master/image) 

## References & More details

[Hall of light (Amiga)](http://hol.abime.net/1311)

[Wikipedia](https://en.wikipedia.org/wiki/Tanglewood_(video_game))

[AtariManiai (Atar)](http://www.atarimania.com/game-atari-st-tanglewood_10471.html)

[Colorcomputearchive (Dragon32/Tandy)](http://www.colorcomputerarchive.com/coco/Documents/Manuals/Games/)


</body>

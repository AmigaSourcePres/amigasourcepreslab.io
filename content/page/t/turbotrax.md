---
title: Turbo Trax
date: 2018-06-10
comments: true
---

<body style="background: darkorange; color: black;">


Turbo Trax is a racing game which tries to bring the feeling of electric car races to the computer. The courses feature elements from the toys like crossovers or chicanes, along with a pit stop which can be used to repair damage or refuel. However, the gameplay is action oriented with the main challenge to avoid going too fast into curves.

There are two playing modes: racing against the AI (or another player) or a time trial. The game features five courses and an editor to create more.

NOTE: This page is just a marker until all pending permissions have been resolved.

<img src="/img/microdeal/turbotrax/turbotrax_box_front.jpg" width="400" />


## Development

<img src="/img/microdeal/turbotrax/turbotrax_box_back.jpg" width="400" />

## Open Source?

The game has been available at various sites for many years, but in 2018 it was given permission by Ian Murray-Watson to be released under an open source license, the MIT License. We are still pending an answer from Martin Kenwright and David Morris, so we won't put up any disk images just yet. Former Microdeal publisher John Symes has also given permission. This is an ongoing effort to release the Microdeal/Michtron-titles for legal preservation.

Sadly, any source code for the game has been lost in time, so all we can offer at the moment is the image files for usage in an emulator. 

## Help!!

- Are you a former Microdeal/Michtron contact or worker and happen have an old backup of the source code for this? Please help us and send it to our email. It is totally fine to send it anonymously.
- Do you want to resource the game to restore the source code. Go ahead, we encourage you, don't forget to notify us.

## Trivia

*Turbo Trax was a bit of a joke really. One review said I'd obviously been a long-time fan of Scalextric. Truth was I'd never owned any, and never thought about it. My main concern was - as it usually was then and still is today when I write a book - to solve an intellectual puzzle. With Tanglewood it was to create a graphical adventure which had nothing but the bare minimum of instructions and left the player to find out everything. Of course, the technology wasn't quite up to doing what I had in mind in those days, but Pete Lyons' graphics were brilliant all the same. With TT it was originally just an exercise in using the Amiga's split screen capabilities.* - Ian Murray-Watson

## Many thanks to 

John Symes for giving his moral permission and hints, Ian Murray-Watson, ( ) and () for creating this game, and being permissive and supporting of preserving their work under liberal usage and license.

## Downloads

### Diskimages

Not yet



### Documentation

[Manual in Asciidoc](https://gitlab.com/amigasourcecodepreservation/tanglewood/blob/master/doc/tanglewood_manual.adoc)

[Manual (pdf)](https://gitlab.com/amigasourcecodepreservation/tanglewood/raw/master/doc/tanglewood_manual_amiga.pdf)

### Box Scans

[A list here](https://gitlab.com/amigasourcecodepreservation/tanglewood/tree/master/image) 

## References & More details

[Hall of light (Amiga)](http://hol.abime.net/1530)

[Lemon Amiga](http://www.lemonamiga.com/?game_id=3284)




</body>

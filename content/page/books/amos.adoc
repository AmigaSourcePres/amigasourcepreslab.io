---
title: AMOS
subtitle: The Free AMOS Library
date: 2018-09-03
comments: true
---

link:/page/books/books[Main Book Page]

include::literaturecollections.adoc[]

[.booktable]
[cols="^.<,^.<,^.<",frame="none"]
|===
| | |
| image:/img/cover-maamostmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/mastering-amiga-amos/blob/master/pdf/mastering-amiga-amos-1993-south.pdf",width=200px]
| image:/img/cover-wrpgsa.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/writing-rpgs-in-amos/blob/master/pdf/writing-rpgs-in-amos-1994-peeke.pdf",width=200px]
| image:/img/cover-agmmtmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/game-makers-manual-amos/blob/master/pdf/game-makers-manual-amos-1992-hill.pdf",width=200px]

| Mastering Amiga Amos
| Writing RPGs in AMOS
| Game Makers Manual: Amiga and AMOS

| Phil South, 1993
| Dicon Peeke, 1994
| Stephen Hill, 1992

| image:https://i.creativecommons.org/l/by-sa/4.0/88x31.png[link="http://creativecommons.org/licenses/by-sa/4.0/"]
| image:https://i.creativecommons.org/l/by-sa/4.0/88x31.png[link="http://creativecommons.org/licenses/by-sa/4.0/"]
| image:https://i.creativecommons.org/l/by-sa/4.0/88x31.png[link="http://creativecommons.org/licenses/by-sa/4.0/"] 

| image:/img/cover_stgmgtmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/game-makers-manual-stos/blob/master/pdf/game-makers-manual-stos-1990-hill.pdf",width=200px]
| image:/img/amos-proug-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-pro-compiler-user-guide-1993-hill.pdf",width=200px]
| image:/img/amos-procompilerug-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-pro-user-guide-1992-croucher-hill.pdf",width=200px]

| Game Makers Manual: Atari ST and STOS
| AMOS Pro Compiler User Guide
| AMOS Pro User Guide

| Stephen Hill, 1990
| Stephen Hill, 1993
| Stephen Hill, Mel Croucher 1992

| image:https://i.creativecommons.org/l/by-sa/4.0/88x31.png[link="http://creativecommons.org/licenses/by-sa/4.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]

| image:/img/amos-compilerug-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-compiler-user-guide-1991-hill.pdf",width=200px]
| image:/img/amos-creatorug-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-the-creator-user-guide-1990-hill.pdf",width=200px]
| image:/img/amos-easy-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/easy-amos-user-guide-1992-croucher.pdf",width=200px]

| AMOS Compiler User Guide
| AMOS The Creator User Guide
| Easy AMOS User Guide

| Stephen Hill, 1991
| Stephen Hill, 1990
| Mel Croucher 1992

| image:https://i.creativecommons.org/l/by-sa/4.0/88x31.png[link="http://creativecommons.org/licenses/by-sa/4.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]

| image:/img/amos-pro-supp-tmb.png[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-pro-application-supplement.pdf",width=200px]
| image:/img/amos-pro20-interface-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-pro-v2.0-interface-summary-revision3.pdf",width=200px]
| image:/img/amos-tome-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-tome-seriesiv-1991-fothergill.pdf",width=200px]

| AMOS Pro Application Supplement
| AMOS Pro v2.0 Interface Summary Rev 3
| AMOS Tome Series IV

| 
| 
| Aaron Fothergill, 1991

| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]

| image:/img/amos-3d-quick-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-3d-quickcard-1991.pdf",width=200px]
| image:/img/amos-3d-ug-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/blob/master/pdf/amos-3d-user-guide-1991-wilkes-lewis.pdf",width=200px]
| image:/img/amos-club-newsletter-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation/tree/master/pdf/amos-club-newsletters",width=200px]

| AMOS 3D Quickcard Ref
| AMOS 3D User Guide
| AMOS Club Newsletter

| 
| Anthony Wilkes, Richard Lewis, 1991 
| Aaron Fothergill

| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]

| image:/img/cover-amos-action-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-in-action/blob/master/pdf/amos-in-action-1992-tucker.pdf",width=200px]
| image:/img/cover-amos-education-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/amos-in-education/blob/master/pdf/amos-in-education-1993-tucker.pdf",width=200px]
| image:/img/cover-key-to-amos-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/key-to-amos/tree/master/pdf/nyckeln-till-amos-1994-holmstrom.pdf",width=200px]

| AMOS In Action
| AMOS In Education
| Nyckeln till AMOS (SE)

| Anne Tucker, Len Tucker, 1992 
| Anne Tucker, Len Tucker, 1993 
| Patrik Holmström, 1994

| https://amigasourcecodepreservation.gitlab.io/amos-in-action/[Online version]
|
|

| https://gitlab.com/amigasourcecodepreservation/amos-in-action/blob/master/asciidoc/book.adoc[Asciidoc]
|
|

| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]
| image:https://i.creativecommons.org/p/mark/1.0/88x31.png[link="http://creativecommons.org/publicdomain/zero/1.0/"]
| image:https://i.creativecommons.org/l/by-sa/4.0/88x31.png[link="http://creativecommons.org/licenses/by-sa/4.0/"]

| image:/img/cover-ultimate-amos-tmb.jpg[role="boxshadow",link="https://gitlab.com/amigasourcecodepreservation/ultimate-amos/blob/master/pdf/ultimate-amos-1994-holborn.pdf",width=200px]
|
|

| Ultimate AMOS
|
|

| Jason Holborn, 1994
|
|

| https://amigasourcecodepreservation.gitlab.io/ultimate-amos/[Online version]
|
|

| https://gitlab.com/amigasourcecodepreservation/ultimate-amos/blob/master/asciidoc/ultimate-amos-1994-holborn.adoc[Asciidoc]
|
|

| image:https://i.creativecommons.org/l/by-sa/4.0/88x31.png[link="http://creativecommons.org/licenses/by-sa/4.0/"]
|
|

|===

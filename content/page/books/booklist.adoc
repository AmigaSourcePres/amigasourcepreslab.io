---
title: Book List
subtitle: Amiga Books Reference
date: 2018-05-08
comments: true
---

= Amiga Reference Book list

== Abacus Software Inc

* https://gitlab.com/amigasourcecodepreservation/amiga-for-beginners[Amiga for Beginners] - 1991 - Christian Spanik - ISBN 0-55755-021-2
* https://gitlab.com/amigasourcecodepreservation/amigabasic-inside-and-ou[Amiga BASIC Inside and Out] - 1988 - Hannes Rugheimer, Christian Spanik - ISBN 0916439879
* Book #3 - Amiga 3D Graphics Programming in Basic - Jennrich, Massman, Schulz - ISBN 1557550441
* Book #4 - Amiga Machine Language - Stefan Dittrich - ISBN 1557550255
* Book #5 - Amiga Tricks & Tips - Bleek, Maelger, Weltner
* Book #6 - Amiga System Programmer's Guide - by Dittrich Gelfand Schemmel - Aug 1988 - 438 pages - ISBN 1557550344
* https://gitlab.com/amigasourcecodepreservation/advanced-system-programmers-guide-for-the-amiga[Advanced System Programmer's Guide for the Amiga] - 1989 - Wolf-Gideon Bleek, Bruno Jennrich, Peter Schulz - ISBN 1557550344
* https://gitlab.com/amigasourcecodepreservation/amigados-inside-and-out[AmigaDOS Inside & Out] - 1990 - Ruediger Kerkloh, Manfred Tornsdorf, Bernd Zoller  - ISBN
* Book #9 - Amiga Disk Drives Inside & Out - Grote Gelfland Abraham - 1989 - ISBN 1557550425
* https://gitlab.com/amigasourcecodepreservation/amiga-c-for-beginners/blob/master/pdf/amiga-c-for-beginners-1987-schaun.pdf[Amiga C for Beginners] - 1987 - Dirk Schaun - ISBN 155755045X
* https://gitlab.com/amigasourcecodepreservation/amiga-c-for-advanced-programmers/blob/master/pdf/amiga-c-for-advanced-programmers-1989-bleek-jennrich-schulz.pdf[Amiga c for advanced programmers]- Bleek Jenrich Schulz - 1989 - ISBN 1557550468
* Book #12 - More Tips & Tricks for the Amiga - Bleek, Maelger, Weltner
* https://gitlab.com/amigasourcecodepreservation/amiga-graphics-inside-and-out[Amiga Graphics Inside and Out] - by Weltner Trapp Jennrich - ISBN 1557550522
* Book #14 - Amiga Desktop Video Guide - Dec 1989 - Guy Wright 
* https://gitlab.com/amigasourcecodepreservation/amiga-printers-inside-and-out/blob/master/pdf/amiga-printers-inside-and-out-1990-ockenfelds.pdf[Amiga Printers Inside & Out] - 1990 - Ralf Ockenfelds - ISBN 1557550875
* Book #16 - Making Music on the Amiga - Thomas Tai, Holger Hahn, Christian Spanik
* Book #17 - The Best Amiga Tricks & Tips - Bleek, Maelger, Weltner
* Amiga Desktop Video Power - Guy Wright
* https://gitlab.com/amigasourcecodepreservation/amigados-quick-reference[AmigaDOS Quick Reference]
* Amiga Intern - Christian Kuhnert, Stefan Maelger, and Johannes Schemmel - 1992 - ISBN 1557551480
* Using AREXX on the Amiga - Chris Zamara and Nick Sullivan - 1991 - ISBN 1557551146

== Ariadne Software Ltd

* The 'Kickstart' Guide to the Amiga 

== Arrays

* https://gitlab.com/amigasourcecodepreservation/mastering-amigados[Mastering Amigados]- Jeffery Stanton and Dan Pinal - 1986 - ISBN 0912003553 - Creative Commons 2018
* The Complete Commodore and Amiga Sourcebook - Jeffery Stanton - ISBN 0912003537]

== Bookmarks Publishing

* https://gitlab.com/amigasourcecodepreservation/first-steps-amiga-surfin/blob/master/pdf/first-steps-amiga-surfin-1996-jeacle.pdf[First Steps Amiga Surfin] - Karl Jeacle - 1996 - ISBN 1855500078 - Creative Commons 2018
* First Steps Amiga: The Ice Cool Guide for Absolute Beginners - Paul Overaa - ISBN  1855500086

== Europress

* https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation[AMOS Pro Compiler User Guide] - 1993 - Stephen Hill - Public Domain 2018
* https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation[AMOS Pro User Guide] - 1992 - Stephen Hill, Mel Croucher - Public Domain 2018
* https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation[AMOS Compiler User Guide] - 1991 - Stephen Hill - Public Domain 2018
* https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation[AMOS The Creator] - Stephen Hill - 1990 - Public Domain 2018
* https://gitlab.com/amigasourcecodepreservation/amos-classic-documentation[Easy AMOS User Guide] - Mel Croucher - 1992 - Public Domain 2018

== Bantam Books / Bantham Doubpleday Dell

* AmigaDOS Developer's Manual
* AmigaDOS Technical Reference Manual
* AmigaDOS User's Manual
* https://gitlab.com/amigasourcecodepreservation/amiga-users-guide-to-graphics-sound-and-telecommunication/blob/master/pdf/amiga-users-guide-to-graphics-sound-and-telecommunications-1987-myers.pdf[Amiga User's Guide to Graphics, Sound and Telecommunications] - Dave Myers, Albert B. Myers - 1987 - ISBN 0553342835 - Creative Commons 2018
* The AmigaDOS Manual - by Commodore Amiga Inc - March 1986 - ISBN 0553342940 

== Bruce Smith Books

=== Standalone books

* All about the A1200, Produced in association with Amiga Computing
* https://gitlab.com/amigasourcecodepreservation/amiga-gamers-guide-volume-one/blob/master/pdf/amiga-gamers-guide-volume-one-1993-smith-slingsby.pdf[Amiga Gamer's Guide - Volume 1] - Dan Slingsby - 1993 - ISBN 1873308167
* Amiga Workbench Booster Pack - by Bruce Smith and Paul Andreas Overaa - Feb 1996 - 512 pages - ISBN 1873308418 

=== Insider Guide Series

* https://amigasourcecodepreservation.gitlab.io/amiga-assembler-insider-guide/[Amiga Assembler] - Paul A. Overaa - 1994 - ISBN 1873308272 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/amiga-disks-and-drives-insider-guide/tree/master[Amiga Disks and Drives] - Paul Overaa - 1994 - ISBN 1873308345 - Creative Commons 2018
* Insider Guide Series: Amiga 600 - by Bruce Smith - Jan 1993 - 256 pages - ISBN 1873308140
* https://gitlab.com/amigasourcecodepreservation/amiga-a1200-insider-guide[Amiga 1200] - Bruce Smith - 1993 - ISBN 1873308159 - Creatice Commons 2018
* Insider Guide Series: Amiga 1200 Next Steps - by Peter Fitzpatrick - Nov 1993 - 256 pages - ISBN 1873308248
* Insider Guide Series: Amiga Workbench 3 A-Z - by Bruce Smith - May 1994 - 256 pages - ISBN 1873308280 

===    Mastering Amiga series

* https://gitlab.com/amigasourcecodepreservation/mastering-the-amiga[Mastering the Amiga] - Bruce Smith, Phil Morse, Alan Jones - 1992 - ISBN 1 897628 0 2 1 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/mastering-amiga-amos/tree/master[Mastering Amiga AMOS] - Phil South - 1993 - Hardback: ISBN 1873308191 Paperback: ISBN 1873308124 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/mastering-amiga-arexx/blob/master/pdf/mastering-amiga-arexx-1993-overaa.pdf[Mastering Amiga ARexx] - Paul Overaa - 1993 - ISBN 1873308132 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/mastering-amiga-assembler[Mastering Amiga Assembler] - Paul A. Overaa - 1992 - ISBN 1873308116 - Creative Commons 2018
* Mastering Amiga Beginners - by Bruce Smith and Mark Webb (listed as by Phil South on Amazon but not in book) - Dec 1991 - 320 pages - ISBN 1873308035
* Mastering Amiga C. - by Paul Andreas Overaa - May 1991 - 320 pages - ISBN 1873308043
* Mastering Amiga DOS 2 - Volume 1 - by Bruce Smith and Mark Smiddy - August 1992; 2nd edition - 416 pages - ISBN 1873308108
* Mastering Amiga DOS 2 - Volume 2 - by Bruce Smith and Mark Smiddy - Forward by Barry Thurston, Technical Director, Commodore Business Machines (UK) Ltd. - August 1992; 2nd edition - 368 pages - ISBN 1873308094
* https://gitlab.com/amigasourcecodepreservation/mastering-amigados-3-reference[Mastering AmigaDOS 3 Reference] - Mark Smiddy - 1993 - ISBN 1873308183 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/mastering-amigados-3-tutorial[Mastering AmigaDOS 3 Tutorial]- Mark Smiddy and Bruce Smith -1993 - ISBN 1873308205 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/mastering-amigados-scripts[Mastering AmigaDOS Scripts]- Mark Smiddy -1994 - ISBN 187330836 - Creative Commons 2018
* Mastering Amiga Printers - by Robin Burton - May 1992 - 336 pages - ISBN 1873308051
* https://gitlab.com/amigasourcecodepreservation/mastering-amiga-programming-secrets[Mastering Amiga Programming Secrets] - Paul Overaa - 1995 - ISBN 1873308337 - Creative Commons 2018
* Mastering Amiga Systems - by Paul Andreas Overaa - April 1992 - 398 pages - ISBN 187330806X
* Mastering Amiga Workbench 2 - by Bruce Smith - Sept 1992 - 320 pages - ISBN 1873308086 


=== Total Amiga Series

* https://amigasourcecodepreservation.gitlab.io/total-amiga-assembler/[Total Amiga Assembler] - Paul A. Overaa - 1995 -  ISBN 1873308574 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/total-amiga-workbench[Total Amiga Workbench] - Bruce Smith - 1995 - ISBN 1873308558 - Creative Commons 2018

== Heim-Verlag

* https://gitlab.com/amigasourcecodepreservation/spiele-programmierung-in-assembler/tree/master[Spiele-programmierung in assembler] - Jorgo Schimanski - 1991 - ISBN 3-928480-02-2
* https://gitlab.com/amigasourcecodepreservation/grafik-in-assembler-aud-dem-amiga[Grafik in Assembler Aud Dem Amiga] - Jorgo Schimanski - 1990 - Creative Commons 2018 - ISBN 3-923250-90-8

== Commodore Business Machines

=== A1000

* Amiga Pocket Reference Guide
* Introduction to the Amiga
* Miscellaneous A1000 manuals
* A1000 Service Manual 

=== A500

* Amiga 500 Binder)
* A500 Service Manual
* A different A500 Service Manual
* Amiga 500 System Schematics
* Amiga Basic
* Selection of A500 books
* A500 Manual-English
* A500 Manual - German
* A520 Video Adapter Manual
* Amiga Basic Book, circa 1985
* Introduction to the Amiga 500 

=== A500/A2000

* Amiga 500/2000 Technical Reference Manuals
* Amiga 500/2000 Basic- English & German

=== A2000/2500

* Amiga 2000 System Schematics
* A2000 Quick Connect guide
* Introduction to the Amiga 2000 3 Editions.
* Introduction to the Amiga 2000HD
* Introduction to the Amiga 2500
* Workbench 1.1 Binder
* Workbench 2.04 Binder
* Amiga Release 2 (German), 1991 

=== A500+

* A500 Plus Service Manual, includes A501 RAM expander (October 1991) 

=== A600

* Amiga 600 Service Addendum
* Introduction to Amiga 600
* Introduction au Amiga 600
* 'Using the Amiga Workbench' & 'Introducing the A600'
* Using the Amiga Workbench 

=== A3000/T

* A3000 System Schematics
* Introducing the Amiga 3000
* Amiga 3000 Service Manual
* Amiga 3000T Service Manual 

=== AGA

* AGA Supplement
* Amiga 1200 Service Addendum
* Amiga 4000 Service Addendum
* A1200 User Guide
* Amiga 4000 French Manuals 

=== CD32

* German CD32 brochure, included with machine 

=== AmigaOS

* A500 WB1.3 Manuals-French
* Workbench 1.2 Enhancer Software& disk
* Workbench 1.3 Enhancer Software & disk
* Using the Amiga Workbench (2.0)
* Amiga Workbench 2.1 and Case
* Amiga Workbench 2.1 Manuals
* German WB2.1 manuals
* Amiga OS 3.0 Manuals
* Workbench 3.0 Manual
* Commodore AmigaOS 3.1 Manuals
* Amiga Technologies AmigaOS 3.1 Manuals
* ARexx User's Guide 

=== General

* Das Grobe Amiga Jahrbuch 1987 - official catalogue
* Amiga Guide Book- Using the Amiga Software
* Amiga Hardware Reference Manual
* Amiga Hardware Reference Manual- 3rd Edition
* Amiga MCC Pascal Manual
* Amiga Intuition Reference Manual
* Amiga ROM Kernel Reference Manual: EXEC
* Amiga ROM Kernel Reference Manual: Libraries and Devices
* Amiga User Guide Binder
* Collection of Amiga Reference Manuals
* Three Amiga ROM Kernel Reference Manuals- Revised & Updated
* Amiga ROM Kernel Reference Manuals: Libraries and Devices- Revised & Updated
* Amiga ROM Kernel Manuals- 3rd Edition
* Amiga ROM Kernel: Devices (3rd Edition)
* Amiga ROM Kernel: Includes & Autodocs (3rd Edition)
* Amiga ROM Kernel: Libraries (3rd Edition)
* Amiga User Interface Style Guide
* Amiga Vision Manual & disks (32.2k)
* Amiga 590 hard drive Service Manual, August 1989
* Amiga 2060/2065/2232 System Schematics Manual, August 1989
* Commodore 1940/2 Monitor Manual
* Commodore 1950 Multiscan Color Monitor Service Manual (June 1990)
* Commodore Amiga 2088 Bridgeboard User's Guide
* Commodore A2091 System Schematics
* Genlock Service Manual, June 1987 

=== Unix

* A3000 Unix manuals 

== Compute! (Compute! Publications, Inc.)

* Advanced Amiga Basic by Tom R. Halfhill and Charles Brannon
* AmigaDOS Reference Guide (1st Edition)
* Amiga DOS Reference Guide (3rd Edition)
* AmigaDOS Reference Guide (4th Edition) by Sheldon Leemon
* Compute!'s Amiga Applications
* Compute!'s Amiga Machine Language Programming Guide
* Compute!'s Amiga Programmers Guide
* Compute!'s Beginner's Guide to the Amiga - by Dan McNeill - February 1986 - 256 or 316 pages (need to verify) - Cover Image:Front - ISBN 0874550254
* Elementary Amiga Basic
* Compute's First Book of the Amiga - 1986
* Compute's Second Book of the Amiga
* Inside Amiga Graphics - by Sheldon Leeman - July 1986 - 303 pages - Cover Image:Front - ISBN 0874550408
* Kids and the Amiga - 2 editiions
* Learning C Programming Graphics on the Amiga & Atari ST
* Mapping the Amiga (2 editions) by Rhett Anderson & Randy Thompson
* Using Deluxe Paint (2nd Edition) 

== Dabs Press

* https://gitlab.com/amigasourcecodepreservation/amigados-a-dabhand-guide/tree/master[AmigaDOS: A Dabhand Guide] - Mark Burgess - 1988 - ISBN - Creative Commons 2018 



== Future Publishing

* Amiga Format Presents: Get into CD-ROM with your A1200 Start-up Guide
* Amiga Format Presents: Get into the Net with your Amiga Start-up Guide
* Amiga Format ScreenPlay 2
* https://gitlab.com/amigasourcecodepreservation/complete-amiga-c[Complete Amiga C] - Cliff Ramshaw - 1994 - ISBN 1898275106 - Creative Commons 2019
* The Amiga Format Bumper Book of Amiga Hints and Tips
* The Amiga Format Guide to Amiga DeskTop Video
* The Amiga Shopper PD Directory - by Ian Wrigley, Phil South and Jason Holborn - Dec 1993 - 526 pages - ISBN 1898275114
* https://gitlab.com/amigasourcecodepreservation/ultimate-amos[Ultimate AMOS] Jason Holborn - 1994 - ISBN 1898275025 - Creative Commons 2019

== Verlag Gabriele Lechner

* https://gitlab.com/amigasourcecodepreservation/amiga-assembler-von-null-auf-hundert[Amiga Assembler Von Null auf Hundert] - Roland Webers, Frank Zavelberg - 1993 - ISBN - Creative Commons 2019

== Haage & Partner

* OS3.9 - Das Buch, 2000 

== Microsoft Press

* https://gitlab.com/amigasourcecodepreservation/amiga-images-sounds-and-animation[The Amiga: Images, Sounds, and Animation on the Commodore Amiga] - Michael Boom - 1986 - ISBN 0914845624 - Creative Commons 2018

== Waite Group Press

* Inside the Amiga with C (2nd edition), Waite Group Press, 1988. ISBN 0-672-22625-1 

== IDG

* Official Amiga World AmigaDOS 2 Companion
* Official Amiga World AmigaVision Handbook 

== IDGC/Peterborough

* https://gitlab.com/amigasourcecodepreservation/the-amiga-companion[The Amiga Companion] - Robert A. Peck - 1988 - ISBN 978-0928579000 - Creative Commons 2018

== Kuma Publishing Ltd

* https://gitlab.com/amigasourcecodepreservation/amos-in-action[AMOS in Action: A Practical Guide to Mastering AMOS on the Amiga] - Anne Tucker and Len Tucker - 1993 - ISBN 074570221X - Public Domain 2018
* https://gitlab.com/amigasourcecodepreservation/amos-in-education[AMOS in Education] - Anne Tucker, Len Tucker - 1993 - 0745702252 - Public Domain 2018
* https://gitlab.com/amigasourcecodepreservation/program-design-techniques-for-the-amiga/tree/master[Program Design Techniques for the Amiga] - Paul Overaa - 1991 - ISBN 0745700322 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/little-blue-workbench-2.0-book[Little Blue Workbench Book] - Mark Smiddy - 1992 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/writing-rpgs-in-amos/tree/master[Writing Role Playing Games in AMOS] - Dicon Peeke - 1994 - 0745702473 - Creative Commons 2018

== Intangible Assets Manufacturing

* Connect Your Amiga! by Dale L. Larson 


== Microsearch

* The Amiga Desktop Video Workbook by Jay Gross 

== Motorola

* M68040 Manual
* M68060 Manual 

== Newtek

* Newtek Video Toaster Font Guide 

== Osborne

* Rebol: The Official Guide 

== Precision Books

* The Amiga System: An Introduction 

== Que

* Amiga Programming Guide - Covers Commodore Amiga Basic, DOS, and C. Softbound, 281 pages. 

== SAMS (Howard W. Sams & Co.)

* Inside the Amiga - by John Thomas Berry - 1986 - 426 pages - Cover Image:Front - ISBN 0672224682 

== Sassenrath Research

* Guru's Guide to The Commodore Amiga- Meditation #1- Interrupts, by Carl Sassenrath 

== Scott, Foresman and Company

* https://gitlab.com/amigasourcecodepreservation/the-amiga-microsoft-basic-programmers-guide/tree/master[The Amiga Microsoft BASIC Programmers Guide] - William B. Sanders - 1987 - ISBN 0673185230 - Creative Commons 2018
* https://gitlab.com/amigasourcecodepreservation/becoming-an-amiga-artist/tree/master[Becoming an Amiga artist] - Vahe Guzelimian, Norbert K.Kuhnert, Gia L. Rozellis - 1987 - ISBN 0673185230 - Creative Commons 2019


== Sybex

* Amiga Programmer's Handbook
* Amiga Programmer's Handbook Volume 1", Second Edition, by Eugene P. Mortimore.k
* Amiga Programmer's Handbook Volume 2, by Eugene P. Mortimore.
* https://gitlab.com/amigasourcecodepreservation/programmers-guide-to-the-amiga[Programmer's Guide to the Amiga] - Robert A. Peck - 1987 - ISBN 0-89588-310-4 - Creative Commons 2018 

== Tab Books, Inc

* Amiga Assembly Language Programming 


== Verlag

* Amiga Professional Results with Deluxe Paint Hard cover book discussing DPaint 2. 400 pages, copyright 1987. 

== Whitestone

* https://gitlab.com/amigasourcecodepreservation/arexx-cookbook/tree/master[The Arexx Cookbook] - Merrill Callaway - 1992 - ISBN 0-9632733-0-8 - Creative Commons 2018

== Sigma Press

* https://gitlab.com/amigasourcecodepreservation/amiga-real-time-3d-graphics[Amiga Real-Time 3d Graphics] - Andrew Tyler - 1992 - ISBN 781850 582755 - Creative Commons 2018
* https://amigasourcecodepreservation.gitlab.io/computers-and-chaos-amiga-edition/[Computer And Chaos Amiga Ed] - Conrad Bessant - 1993 - ISBN 1850582831
* https://gitlab.com/amigasourcecodepreservation/game-makers-manual-amos[Game Makers Manual Amos] - Stephen Hill - 1992 - 1850582300 - Creative Commons 2018

== Other

* https://gitlab.com/amigasourcecodepreservation/multimedia-with-the-amiga[Multimedia with the Amiga] - Dave Johnson - 1991 - ISBN 978-1556222153 - Creative Commons 2019
* Amiga Desktop Video by Steven Anzovin- 2nd Edition
* Amiga Hardware Tuning
* Amiga Programmer's Guide
* ARexx User's Reference Manual 1.0 By William S. Hawes, 1987
* 1001 Things to do with your Amiga
* Best Amiga Tips & Secrets
* Complete Post Production with the Video Toaster
* Mastering Toaster Technology
* Starter- Amiga 500
* The Amiga Handbook
* https://gitlab.com/amigasourcecodepreservation/understanding-imagine-2.0/blob/master/pdf/understanding-imagine-2.0-1992-worley.pdf[Understanding Imagine 2.0] - Steven Worley - 1992
* https://gitlab.com/amigasourcecodepreservation/the-imagine-companion/blob/master/pdf/the-imagine-companion-1991-duberman.pdf[The Imagine Companion] - David Duberman - 1991 
* The Amiga Guru Book - Ralph Babel, 1993 - No ISBN

== Avid Publications

* Video Toaster User Presents 101 Toaster Tricks by Lee Stranahan 30 page booklet. Published in 1992-93 

== German language

=== Technic Support

* Das grosse Amiga Public Domain Buch
* Das Zweite Amiga Public Domain Buch
* Das Grosse Amiga Spielebuch 

=== Markt & Technik

* Amiga DOS 1.3
* Amiga Total, 1992
* Amiga 500 Buch
* Amiga Workshop
* Amiga Reflections
* Das Amiga Handbuch
* Deluxe Grafik mit dem Amiga 

=== Memphis Computer Products GmbH

* Das Imagine Buch 

=== Data Becker

* https://gitlab.com/amigasourcecodepreservation/amiga-basic-spanik-rugheimer/blob/master/pdf/amiga-basic-1986-spanik-rugheimer.pdf[Amiga Basic] - Hannes Rügheimer; Christian Spanik - 1986- ISBN - Creative Commons 2019
* https://gitlab.com/amigasourcecodepreservation/amiga-500-fur-einsteiger/blob/master/pdf/amiga-500-fur-einsteiger-1987-spanik.pdf[Amiga 500 Fur Einsteiger] - Christian Spanik - 1987- ISBN - Creative Commons 2019
* AmigaDOS
* Amiga Profi Know How, 1991
* BeckerText 2
* Das endgültige Amiga 500 Handbuch, 1991
* Das Grobe Amiga 500 buch
* Das große Deluxe Paint IV Buch, 1992
* Demomaker handbook, 1991
* German Amiga DOS/Amiga Basic book
* Tips & Tricks, 2nd edition)
* https://gitlab.com/amigasourcecodepreservation/maschinensprache-fuer-einsteiger/blob/master/pdf/maschinensprache-fuer-einsteiger-1990-tornsdorf.pdf[Maschinesprache fuer einsteiger] - Manfred Tornsdorf - 1990 - Creative Commons
* https://gitlab.com/amigasourcecodepreservation/das-grosse-amiga-2000-buch/blob/master/pdf/das-grosse-amiga-2000-buch-1987-spanik-rugheimer.pdf[Das grosse Amiga 2000 Buch] - Hannes Rügheimer; Christian Spanik - 1987 - Creative Commons 2019

=== Eberlein

* Blitzeinstieg Kickstart 2.0, 1991 

=== Swedish language

== Elevdata

* https://gitlab.com/amigasourcecodepreservation/key-to-amos[Nyckeln till AMOS] - 1994 - Patrik Holmström - Creative Commons 2018

== Did these reach the printers?

* Total Amiga Amigados - By Bruce Smith - Oct 1995 - 416 pages - ISBN 1873308566
* Blitz BASIC for the Amiga: A Complete Programming Guide - Neil Wright - 1995 - ISBN	1850585377, 9781850585374
* AMOS in Action: A Practical Guide to Mastering AMOS on the Amiga - 1992 - Software Developmentsee same book published by Kuma Publishing 1993 

== Manuals (infinite list)

* https://gitlab.com/amigasourcecodepreservation/assempro-manual/blob/master/pdf/assempro-manual-1990-schulz.pdf[AssemPro] - Peter Schulz - 1990 - Creative Commons 2019
* ABSoft AC/Basic- 2nd Edition 
* https://gitlab.com/amigasourcecodepreservation/professional-calc[Professional Calc] - Creative Commons 2018
* Deluxe Paint IV Manual 

=== Gamers books - BSB

* Secrets of Frontier Elite - by Tony Dillon - Sept 1994 - 128 pages - ISBN 1873308396
* Secrets of Sim City 2000 - by Andrew Banner - Feb 1995 - 128 pages - ISBN 1873308477 
